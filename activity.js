db.rooms.updateOne(
    { name: "single" },
    {
        $set: {
            rooms_available: "5",
            accomodates: "2",
            price: "1000",
             description: "A room fit for a small family going on a vacation"
            rooms_available: "5",
            isAvailable: "false"  
        }
    }
);


db.rooms.updateMany(
    { name: "double" },
    {
        $set: {
            rooms_available: "5",
            accomodates: "3",
            price: "2000",
             description: "A room fit for a small family going on a vacation"
            rooms_available: "5",
            isAvailable: "false"  
        }
    }
);

db.rooms.updateMany(
    { name: "queen" },
    {
        $set: {
            rooms_available: "15",
            accomodates: "4",
            price: "4000",
             description: "A room with a queen sized bed perfect for a simple getaway "
            rooms_available: "15",
            isAvailable: "false"  
        }
    }
);


db.rooms.find({ name: "double" });


db.rooms.updateOne(
    { name: "queen" },
    {
        $set: {
            rooms_available: "0"
        }
    }
);



db.users.deleteMany({
    rooms_available: "0"
});